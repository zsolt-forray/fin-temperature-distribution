#!/usr/bin/python3


"""
Chart settings
"""


__author__  = 'Zsolt Forray'
__license__ = 'MIT'
__version__ = '0.0.1'
__date__    = '26/11/2019'
__status__  = 'Development'


import os
import json
from matplotlib import pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons


class ChartSettings:
    def __init__(self):
        # JSON file location
        self.json_loc = os.path.join(os.path.dirname(os.path.abspath(__file__)), "json")
        # Label axes position
        self.text_axes_pos = (0.055, 0.15, 0.5, 0.1)
        # Chart labels
        self.titles = { "title":"Temperature Distribution in a Pin Fin",
                        "ylabel":"Temperature, °C",
                        "xlabel":"Distance from the base, mm",
                        }

    @staticmethod
    def set_axes(*pos):
        axes = plt.axes([*pos])
        return axes

    @staticmethod
    def set_sliders_params(params):
        positions = params["left"], params["bottom"], params["width"], params["height"]
        return Slider(ChartSettings.set_axes(*positions), params["text"],\
                                    params["min"], params["max"],\
                                    params["init"], params["valfmt"],\
                                    color=params["color"])
    @staticmethod
    def set_radio_params(params):
        positions = params["left"], params["bottom"], params["width"], params["height"]
        return RadioButtons(ChartSettings.set_axes(*positions), params["text"], params["active"])

    @staticmethod
    def set_buttons_params(params):
        positions = params["left"], params["bottom"], params["width"], params["height"]
        return Button(ChartSettings.set_axes(*positions), params["text"],\
                                    color=params["color"], hovercolor=params["hovercolor"])

    def set_labels_params(self, params, text):
        positions = params["x"], params["y"]
        return self.ax_text.text(*positions, text, size=params["size"],\
                                 style=params["style"], color=params["color"],\
                                 backgroundcolor=params["bgcolor"],\
                                 weight=params["weight"])

    def set_sliders(self):
        # Sliders
        with open(os.path.join(self.json_loc, "sliders_parameters.json")) as json_file:
            slider_params = json.load(json_file)

        # Slider for base temperature
        params = slider_params["T1sl"]
        self.T1sl = ChartSettings.set_sliders_params(params)

        # Slider for air temperature
        params = slider_params["T2sl"]
        self.T2sl = ChartSettings.set_sliders_params(params)

        # Slider for fin length
        params = slider_params["length"]
        self.length = ChartSettings.set_sliders_params(params)

        # Slider for fin diameter
        params = slider_params["dia"]
        self.dia = ChartSettings.set_sliders_params(params)

    def set_radiobuttons(self):
        # RadioButtons
        with open(os.path.join(self.json_loc, "radio_parameters.json")) as json_file:
            radio_params = json.load(json_file)

        # RadioButtons for material selector
        params = radio_params["material_selector"]
        self.radio = ChartSettings.set_radio_params(params)

    def set_buttons(self):
        # Buttons
        with open(os.path.join(self.json_loc, "button_parameters.json")) as json_file:
            button_params = json.load(json_file)

        # Button for close
        params = button_params["close"]
        self.button_close = ChartSettings.set_buttons_params(params)

    def set_labels(self):
        self.ax_text = ChartSettings.set_axes(*self.text_axes_pos)
        self.ax_text.axis("off")

        # Labels
        with open(os.path.join(self.json_loc, "label_parameters.json")) as json_file:
            label_params = json.load(json_file)

        params = label_params["materials_label"]
        self.set_labels_params(params, "Materials")

        params = label_params["base_temp_label"]
        self.set_labels_params(params, "Base Temp. °C")

        params = label_params["air_temp_label"]
        self.set_labels_params(params, "Air Temp. °C")

        params = label_params["length_label"]
        self.set_labels_params(params, "Length (mm)")

        params = label_params["dia_label"]
        self.set_labels_params(params, "Diameter (mm)")

        params = label_params["result"]
        self.res_text = ("Total heat transfer: {:>6.2f} W\n\n"
                         "Fin efficiency: {:>16.2%}")
        self.tbox = self.set_labels_params(params, self.res_text\
                                           .format(self.qadi, self.eff))

    def set_chart(self):
        self.fig = plt.figure(figsize=(9,6))
        self.ax = self.fig.add_subplot(1, 1, 1)
        # General
        self.ax.grid(False)
        plt.title(self.titles["title"])
        plt.ylabel(self.titles["ylabel"])
        plt.xlabel(self.titles["xlabel"])
        plt.subplots_adjust(bottom=0.15, left=0.4, right=0.95)
        # Widgets
        self.set_sliders()
        self.set_radiobuttons()
        self.set_buttons()
        self.set_labels()
        return self.fig, self.ax
