#!/usr/bin/python3


"""
Chart plotting
"""


__author__  = 'Zsolt Forray'
__license__ = 'MIT'
__version__ = '0.0.1'
__date__    = '24/11/2019'
__status__  = 'Development'


from matplotlib import pyplot as plt
from chart_settings import ChartSettings


class ChartPlotting(ChartSettings):
    def __init__(self):
        ChartSettings.__init__(self)

    def run_plot(self):
        self.set_chart()
        self.ax.set_xlim(self.xr[0], self.xr[-1]*1.1)
        self.l, = self.ax.plot(self.xr, self.Tarr, color="#34495E")
        self.T1sl.on_changed(self.update_temp)
        self.T2sl.on_changed(self.update_temp)
        self.length.on_changed(self.update_geom)
        self.dia.on_changed(self.update_geom)
        self.radio.on_clicked(self.update_material)
        self.button_close.on_clicked(ChartPlotting.close_chart)
        plt.show()

    def update_temp(self, val):
        self.T1 = int(self.T1sl.val)
        self.T2 = int(self.T2sl.val)

        y, q, e = self.run_calc()[1:]
        self.tbox.set_text(self.res_text.format(q, e))
        self.l.set_ydata(y)
        minT_bound = min(y[-1], y[0])*0.99
        maxT_bound = max(y[-1], y[0])*1.01
        self.ax.set_ylim(minT_bound, maxT_bound)
        self.fig.canvas.draw_idle()

    def update_geom(self, val):
        self.L = int(self.length.val) / 1000
        self.d = int(self.dia.val) / 1000

        x, y, q, e = self.run_calc()
        self.tbox.set_text(self.res_text.format(q, e))
        self.l.set_ydata(y)
        self.l.set_xdata(x)
        self.ax.set_xlim(x[0], x[-1]*1.1)
        minT_bound = min(y[-1], y[0])*0.99
        maxT_bound = max(y[-1], y[0])*1.01
        self.ax.set_ylim(minT_bound, maxT_bound)
        self.fig.canvas.draw_idle()

    def update_material(self, label):
        material = label
        conductivities = {
                            "copper":400,
                            "platinum":70,
                            "carbon steel": 54,
                            }
        self.k = conductivities[material]

        y, q, e = self.run_calc()[1:]
        self.tbox.set_text(self.res_text.format(q, e))
        self.l.set_ydata(y)
        minT_bound = min(y[-1], y[0])*0.99
        maxT_bound = max(y[-1], y[0])*1.01
        self.ax.set_ylim(minT_bound, maxT_bound)
        self.fig.canvas.draw_idle()

    @staticmethod
    def close_chart(evt):
        plt.close()
