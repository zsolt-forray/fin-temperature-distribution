#!/usr/bin/python3


"""
This tool calculates the fin efficiency and the temperature as a function of
distance from the base of a pin fin.
The pin fin is attached to a base, which has the same cross-sectional area
and is at a constant temperature.
No heat transfer is considered through the tip surface.

Symbols:
d (m):      pin fin diamater (constant)
h (W/m2K):  convective heat transfer coefficient
k (W/mK):   thermal conductivity
L (m):      fin length
T1 (°C):    fin base temperature
T2 (°C):    ambient air temperature
"""


__author__  = 'Zsolt Forray'
__license__ = 'MIT'
__version__ = '0.0.1'
__date__    = '26/11/2019'
__status__  = 'Development'


import numpy as np
from chart_plotting import ChartPlotting


class FinEfficiency(ChartPlotting):
    def __init__(self):
        self.d = 15      # mm
        self.h = 50      # W/m2K
        self.k = 54      # W/mK
        self.L = 100     # mm
        self.T1 = 100    # base, °C
        self.T2 = 25     # ambient, °C
        self.d = self.d / 1000
        self.L = self.L / 1000

        super().__init__()

    def calc_simplification_terms(self):
        P = np.pi * self.d
        A = pow(self.d, 2) * np.pi / 4
        m = np.sqrt((self.h * P) / (self.k * A)) # simplification term
        M = np.sqrt(self.h * P * self.k * A) * (self.T1 - self.T2) # simplification term
        return m, M

    def calc_total_heat_transfer(self, m, M):
        # Total heat transfer
        return M * np.tanh(m * self.L)

    def calc_efficiency(self, m):
        # Efficiency
        return np.tanh(np.sqrt(2) * m * self.L) / (np.sqrt(2) * m * self.L)

    def calc_chart_points(self, m):
        s = 0.001
        xr = np.arange(0, self.L+s, s)
        Tarr = []
        for x in xr:
            # no heat transfer through the tip surface
            Tadi = np.cosh(m * (self.L - x)) \
                           / np.cosh(m * self.L) * (self.T1 - self.T2) + self.T2
            Tarr.append(Tadi)
        xr = [x * 1000 for x in xr]
        return xr, Tarr

    def run_calc(self):
        m, M = self.calc_simplification_terms()
        self.qadi = self.calc_total_heat_transfer(m, M)
        self.eff = self.calc_efficiency(m)
        self.xr, self.Tarr = self.calc_chart_points(m)
        return self.xr, self.Tarr, self.qadi, self.eff


def run_app():
    feobj = FinEfficiency()
    feobj.run_calc()
    feobj.run_plot()


if __name__ == "__main__":
    run_app()
