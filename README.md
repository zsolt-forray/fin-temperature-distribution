# Fin Temperature Distribution and Fin Efficiency

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/a56862fe0024475b9619ac4456ff7cdd)](https://www.codacy.com/manual/forray.zsolt/fin-temperature-distribution?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=Zsolt-Forray/fin-temperature-distribution&amp;utm_campaign=Badge_Grade)
[![Python 3.7](https://img.shields.io/badge/python-3.7-blue.svg)](https://www.python.org/downloads/release/python-370/)

## Description
This tool calculates the fin efficiency and the temperature as a function of distance from the base of a pin fin. The pin fin is attached to a base, which has the same cross-sectional area and is at a constant temperature. No heat transfer is considered through the tip surface.

## Usage

![Screenshot](/png/fig.png)

### Usage Example

```python
#!/usr/bin/python3

import fin_temperature_distribution as ftd

ftd.run_app()
```

**Initial Parameters:**

* d (mm):     pin fin diamater (constant)
* h (W/m2K):  convective heat transfer coefficient
* k (W/mK):   thermal conductivity
* L (mm):     fin length
* T1 (°C):    fin base temperature
* T2 (°C):    ambient air temperature

## LICENSE
MIT

## Contributions
Contributions to this repository are always welcome.
This repo is maintained by Zsolt Forray (forray.zsolt@gmail.com).
